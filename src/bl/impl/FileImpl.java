package bl.impl;

import bl.FileInterface;
import services.ServiceLocator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileImpl implements FileInterface {
	private ServiceLocator serviceLocator;

	public FileImpl() {
		serviceLocator = ServiceLocator.getLocator();
	}

	@Override
	public void copyFile(File source, File target) {
		NotificationImpl notification = (NotificationImpl) serviceLocator.getService("notificationService");

		notification.sendMessage("Copying " + source.getAbsolutePath() + " to " + target.getAbsolutePath());

		try {
			Files.copy(source.toPath(), target.toPath());
			notification.sendMessage("Copying completed!");
		} catch (IOException e) {
			notification.sendMessage("Copying failed!");
			e.printStackTrace();
		}
	}

}
