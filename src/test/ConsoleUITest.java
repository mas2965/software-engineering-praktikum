package test;

import bl.impl.FileImpl;
import bl.impl.NotificationImpl;
import org.junit.Test;
import services.ServiceLocator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.Assert.*;


public class ConsoleUITest {

    @Test
    public void testCopy() {
        ServiceLocator sl = ServiceLocator.getLocator();
        sl.add("notificationService", new NotificationImpl());
        sl.add("file", new FileImpl());

        NotificationImpl n = (NotificationImpl) sl.getService("notificationService");
        FileImpl f = (FileImpl) sl.getService("file");

        n.sendMessage("Copying...");

        File oldFile = new File("testcopies/toCopy");
        File newFile = new File("testcopies/newCopy");

        try {
            Files.deleteIfExists(newFile.toPath());
        } catch (IOException e) {
            fail("Deleting file failed");
        }

        f.copyFile(oldFile, newFile);

        assertTrue("Copy of file not present", newFile.exists());
    }
}
