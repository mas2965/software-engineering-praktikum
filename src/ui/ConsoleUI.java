package ui;

import bl.impl.FileImpl;
import bl.impl.NotificationImpl;
import services.ServiceLocator;

import java.io.File;
import java.util.Scanner;

public class ConsoleUI {
	public static void main(String[] args) {
		ServiceLocator serviceLocator = ServiceLocator.getLocator();
		serviceLocator.add("notificationService", new NotificationImpl());
		serviceLocator.add("fileService", new FileImpl());

		NotificationImpl notification = (NotificationImpl) serviceLocator.getService("notificationService");
		FileImpl fileService = (FileImpl) serviceLocator.getService("fileService");
		Scanner scanner = new Scanner(System.in);

		notification.sendMessage("Bitte geben Sie den Dateifpad an:");
		String sourcePath = scanner.nextLine();
		File sourceFile = new File(sourcePath);

		notification.sendMessage("Bitte geben Sie den Zielpfad an:");
		String destinationPath = scanner.nextLine();
		File destinationFile = new File(destinationPath);

		fileService.copyFile(sourceFile, destinationFile);
	}
}
