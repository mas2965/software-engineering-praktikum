package services;

import java.util.HashMap;

public class ServiceLocator {
	private static ServiceLocator instance = null;
	private HashMap<String, Object> serviceMap = new HashMap<String, Object>();

	public static ServiceLocator getLocator() {
		if(instance == null) {
			instance = new ServiceLocator();
		}

		return instance;
	}

	public Object getService(String serviceName) {
		return serviceMap.get(serviceName);
	}

	public void add(String serviceName, Object service) {
		serviceMap.put(serviceName, service);
	}
}
